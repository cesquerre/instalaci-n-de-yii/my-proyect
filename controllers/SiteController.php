<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use app\models\LoginForm;
use app\models\ContactForm;

use app\models\Publicaciones;
use app\models\Albumes;
use app\models\Articulos;
use app\models\Conciertos;
use app\models\Merchandising;
use app\models\Canciones;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    
   public function actionAlbumes(){
        $dataProvider = new ActiveDataProvider([
            'query' => Albumes::find(),
        ]);
        
        return $this->render("albumes",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
    public function actionArticulos(){
        $dataProvider = new ActiveDataProvider([
            'query' => Articulos::find(),
        ]);
        
        return $this->render("articulos",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
    public function actionPublicaciones($album_id = null){
        
        $albumes = new ActiveDataProvider([
            'query' => Albumes::find()->all(),
        ]);
       
            $query = Publicaciones::find();
            if ($album_id !== null) {
            $query->andWhere(['idAlbum' => $album_id]);
            }
       
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        
        return $this->render("publicaciones",[
            "albumes"=>$albumes,
            "dataProvider"=>$dataProvider,
        ]);
    }
    
    public function actionConciertos(){
        $dataProvider = new ActiveDataProvider([
            'query' => Conciertos::find(),
        ]);
        
        return $this->render("conciertos",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
    public function actionMerch() {
        $dataProvider = new ActiveDataProvider([
            'query' => Merchandising::find(),
        ]);
        
        return $this->render("merch",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
    public function actionCanciones() {
        $dataProvider = new ActiveDataProvider([
            'query' => Canciones::find(),
        ]);
        
        return $this->render("canciones",[
            "dataProvider"=>$dataProvider,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
