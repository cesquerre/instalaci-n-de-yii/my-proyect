<?php

use app\models\Canciones;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Canciones';
$this->params['breadcrumbs'][] = $this->title;
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mi Sitio - <?= Html::encode($this->title) ?></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/site.css">
    <link rel="icon" type="image/png" href="images/logo.png">
    
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Neucha&display=swap');
        
        *{
            color: #e0e0e0;
            font-family: 'Neucha';
        }
        
        h1{
            text-align: center;
        }
    
        .canciones-index{
            padding-left: 400px;
        }
        
    </style>
</head>

<body>
<div class="canciones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Canciones', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'titulo',
            'duracion',
            'idAlbum',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Canciones $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>
    
    <?= Html::jsFile('https://code.jquery.com/jquery-3.5.1.slim.min.js') ?>
    <?= Html::jsFile('https://cdn.jsdelivr.net/npm/@popperjs/core@2.0.8/dist/umd/popper.min.js') ?>
    <?= Html::jsFile('https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js') ?>

</div>
</body>
</html>