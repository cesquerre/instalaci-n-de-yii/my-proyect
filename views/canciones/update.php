<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Canciones $model */

$this->title = 'Update Canciones: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Canciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<style>
    
    @import url('https://fonts.googleapis.com/css2?family=Neucha&display=swap');
        
    *{
        color: #e0e0e0;
        font-family: 'Neucha';
    }
    
    .canciones-update{
        padding-left: 400px;
    }
    
</style>

<div class="canciones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
