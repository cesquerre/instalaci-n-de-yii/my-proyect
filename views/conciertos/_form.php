<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Conciertos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="conciertos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo')->textInput() ?>

    <?= $form->field($model, 'tour')->dropDownList([ 'Fearless' => 'Fearless', 'Speak Now' => 'Speak Now', 'Red' => 'Red', 1989 => '1989', 'reputation' => 'Reputation', 'Eras' => 'Eras', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'ubicacion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'capacidad')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
