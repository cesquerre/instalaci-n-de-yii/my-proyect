<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\GenerosMusicales $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="generos-musicales-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idAlbum')->textInput() ?>

    <?= $form->field($model, 'genero')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
