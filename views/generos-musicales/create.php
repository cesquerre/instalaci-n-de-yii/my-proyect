<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\GenerosMusicales $model */

$this->title = 'Create Generos Musicales';
$this->params['breadcrumbs'][] = ['label' => 'Generos Musicales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    
    @import url('https://fonts.googleapis.com/css2?family=Neucha&display=swap');
        
    *{
        color: #e0e0e0;
        font-family: 'Neucha';
    }
    
    .generos-musicales-create{
        padding-left: 400px;
    }
    
</style>

<div class="generos-musicales-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
