<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Merchandising $model */

$this->title = 'Create Merchandising';
$this->params['breadcrumbs'][] = ['label' => 'Merchandisings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    
    @import url('https://fonts.googleapis.com/css2?family=Neucha&display=swap');
        
    *{
        color: #e0e0e0;
        font-family: 'Neucha';
    }
    
    .merchandising-create{
        padding-left: 400px;
    }
    
</style>

<div class="merchandising-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
