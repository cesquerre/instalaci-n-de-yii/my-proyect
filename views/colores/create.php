<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Colores $model */

$this->title = 'Create Colores';
$this->params['breadcrumbs'][] = ['label' => 'Colores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    
    @import url('https://fonts.googleapis.com/css2?family=Neucha&display=swap');
        
    *{
        color: #e0e0e0;
        font-family: 'Neucha';
    }
    
    .colores-create{
        padding-left: 400px;
    }
    
</style>

<div class="colores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
