<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Precios $model */

$this->title = 'Create Precios';
$this->params['breadcrumbs'][] = ['label' => 'Precios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    
    @import url('https://fonts.googleapis.com/css2?family=Neucha&display=swap');
        
    *{
        color: #e0e0e0;
        font-family: 'Neucha';
    }
    
    .precios-create{
        padding-left: 400px;
    }
    
</style>

<div class="precios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
