<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Precios $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="precios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idConcierto')->textInput() ?>

    <?= $form->field($model, 'precio')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
