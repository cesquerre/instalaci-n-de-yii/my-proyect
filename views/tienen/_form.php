<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Tienen $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="tienen-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idAlbum')->textInput() ?>

    <?= $form->field($model, 'idMerch')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
