<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Compositores $model */

$this->title = 'Create Compositores';
$this->params['breadcrumbs'][] = ['label' => 'Compositores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    
    @import url('https://fonts.googleapis.com/css2?family=Neucha&display=swap');
        
    *{
        color: #e0e0e0;
        font-family: 'Neucha';
    }
    
    .compositores-create{
        padding-left: 400px;
    }
    
</style>

<div class="compositores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
