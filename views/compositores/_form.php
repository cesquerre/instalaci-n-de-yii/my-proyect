<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Compositores $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="compositores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idCancion')->textInput() ?>

    <?= $form->field($model, 'compositor')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
