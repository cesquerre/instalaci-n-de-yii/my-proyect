<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;

/** @var yii\web\View $this */

$this->title = 'Publicaciones';

?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <!-- Integración de Bootstrap CSS -->
    <?= Html::cssFile('https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css') ?>
    <link rel="stylesheet" href="<?=Url::to('@web/css/publicaciones.css')?>">
    <?php $this->head() ?>

</head>

<style>
    
    h1{
    text-align: center;
}
    
.post-container{
    border-style: solid;
    border-width: 2px;
    padding: 10px;
    background-color: #e0e0e0;
    color: #000;
    font-family: 'Neucha';
}
    
</style>

<body>
    
    <!-- Al seleccionar un álbum, aparecerán las publicaciones que tratan sobre ese álbum -->
    <div class="container-fluid" style="margin-top: 90px; width: 100vw">
        <h1>PUBLICACIONES</h1>
        
        <div class="row">
            <div class="col-sm-3">
                <h4>Selecciona un álbum:</h4>
                
<!--                <div class="d-flex align-items-start">
                    <div class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                      <button class="nav-link active btn-light" id="v-pills-home-tab" data-bs-toggle="pill" data-bs-target="#v-pills-home" type="button" role="tab" aria-controls="v-pills-home" aria-selected="true">Home</button>
                      <button class="nav-link" id="v-pills-profile-tab" data-bs-toggle="pill" data-bs-target="#v-pills-profile" type="button" role="tab" aria-controls="v-pills-profile" aria-selected="false">Profile</button>
                      <button class="nav-link" id="v-pills-disabled-tab" data-bs-toggle="pill" data-bs-target="#v-pills-disabled" type="button" role="tab" aria-controls="v-pills-disabled" aria-selected="false" disabled>Disabled</button>
                      <button class="nav-link" id="v-pills-messages-tab" data-bs-toggle="pill" data-bs-target="#v-pills-messages" type="button" role="tab" aria-controls="v-pills-messages" aria-selected="false">Messages</button>
                      <button class="nav-link" id="v-pills-settings-tab" data-bs-toggle="pill" data-bs-target="#v-pills-settings" type="button" role="tab" aria-controls="v-pills-settings" aria-selected="false">Settings</button>
                    </div>
                    <div class="tab-content" id="v-pills-tabContent">
                      <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab" tabindex="0">...</div>
                      <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab" tabindex="0">...</div>
                      <div class="tab-pane fade" id="v-pills-disabled" role="tabpanel" aria-labelledby="v-pills-disabled-tab" tabindex="0">...</div>
                      <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab" tabindex="0">...</div>
                      <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab" tabindex="0">...</div>
                    </div>
                </div>-->

                <ul class="nav nav-pills flex-column">
                    
                    <li class="nav-item">
                      <a data-toggle="pill" class="nav-link" href="#1">Debut</a>
                    </li>
                    <li class="nav-item">
                      <a data-toggle="pill" class="nav-link" href="#2">Fearless</a>
                    </li>
                    <li class="nav-item">
                      <a data-toggle="pill" class="nav-link" href="#3">Speak Now</a>
                    </li>
                    <li class="nav-item">
                        <a data-toggle="pill" class="nav-link" href="#4">Red</a>
                    </li>
                    <li class="nav-item">
                        <a data-toggle="pill" class="nav-link" href="#5">1989</a>
                    </li>
                    <li class="nav-item">
                        <a data-toggle="pill" class="nav-link" href="#6">reputation</a>
                    </li>
                    <li class="nav-item">
                        <a data-toggle="pill" class="nav-link" href="#7">Lover</a>
                    </li>
                    <li class="nav-item">
                        <a data-toggle="pill" class="nav-link" href="#8">folklore</a>
                    </li>
                    <li class="nav-item">
                        <a data-toggle="pill" class="nav-link" href="#9">evermore</a>
                    </li>
                    <li class="nav-item">
                        <a data-toggle="pill" class="nav-link" href="#10">Midnights</a>
                    </li>
                    <li class="nav-item">
                        <a data-toggle="pill" class="nav-link" href="#11">The Tortured Poets Department</a>
                    </li>
                    <hr class="d-sm-none">
                </ul>
            </div>
            <div class="col-sm-9">
                    <?= ListView::widget([
                            'dataProvider' => $dataProvider,
                            'itemView' => '_publicacion',
                            'layout' => "{items}{pager}",
                        ]);
                    ?>
            </div>
        </div>  
    </div>
   
    
    <!-- Integración de Bootstrap JS y jQuery -->
    <?= Html::jsFile('https://cdn.jsdelivr.net/npm/jquery@3.7.1/dist/jquery.slim.min.js') ?>
    <?= Html::jsFile('https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js') ?>
    <?= Html::jsFile('https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js') ?>
    
   

</body>
</html>

