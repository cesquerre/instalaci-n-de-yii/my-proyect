<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = "Publicaciones";

?>

<link rel="stylesheet" href="<?=Url::to('@web/css/publicaciones.css')?>">

<div class="col-sm-6 col-md-12">
    <div class="post-container" id="#<?= $model->idAlbum ?>" class="tab-pane fade">
        <h2><?= Html::encode($model->titulo) ?></h2>
        <h5>Title description, Dec 7, 2017</h5>
        <p><?= Html::encode($model->resumen) ?></p>
        <button type="button" class="btn btn-dark">Leer más</button>
    </div>
    <br>
</div>