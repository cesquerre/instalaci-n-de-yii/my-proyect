<?php

/** @var yii\web\View $this */

$this->title = 'Merchandising';

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;

?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <!-- Integración de Bootstrap CSS -->
    <?= Html::cssFile('https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css') ?>
    <link rel="stylesheet" href="<?=Url::to('@web/css/publicaciones.css')?>">
    <?php $this->head() ?>

</head>

<style>
    
    h1{
        text-align: center;
    }
    
    
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

<body>
    
    <div class="container" style="margin-top: 90px; width: 100vw">
        <h1>MERCHANDISING</h1>
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemOptions' => ['class' => 'col-md-4'],
                'layout' => '<div class="row">{items}</div>{pager}',
                'itemView' => function ($model, $key, $index, $widget) {
                return $this->render('_merch', ['model' => $model]);
                },
              ]); 
            ?>
        
    </div>
    
    <!-- Integración de Bootstrap JS y jQuery -->
    <?= Html::jsFile('https://cdn.jsdelivr.net/npm/jquery@3.7.1/dist/jquery.slim.min.js') ?>
    <?= Html::jsFile('https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js') ?>
    <?= Html::jsFile('https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js') ?>
    
</body>
</html>

