<?php

/** @var yii\web\View $this */

$this->title = 'Home';

use yii\helpers\Html;

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mi Sitio - <?= Html::encode($this->title) ?></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/home.css">
    <link rel="icon" type="image/png" href="images/logo.png">
   
</head>

<body>

<div class="site-index">

    <div class="body-content" style="width: 100vw">
        
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="images/carousel/imagen3.jpg" class="d-block w-100" alt="Carrusel 1">
                    <div class="carousel-caption d-none d-md-block">
                        <h3>Análisis profundo de letras</h3>
                        <p>Nuestro análisis de letras te llevará más allá de las melodías, revelando las historias y emociones detrás de cada canción.</p>
                    </div>
                </div>
                
            <div class="carousel-item">
                <img src="images/carousel/imagen4.jpg" class="d-block w-100" alt="Carrusel 2">
                <div class="carousel-caption d-none d-md-block">
                    <h3>Publicaciones exclusivas</h3>
                    <p>Mantente al día con las noticias más recientes, lanzamientos de álbumes, y eventos.</p>
                </div>
            </div>
                
            <div class="carousel-item">
                <img src="images/carousel/imagen7.jpg" class="d-block w-100" alt="Carrusel 3">
                <div class="carousel-caption d-none d-md-block">
                    <h3>Impacto mundial de Taylor Swift</h3>
                    <p>Desde sus inicios hasta convertirse en una superestrella global, desglosamos su influencia en cada paso del camino.</p>
                </div>
            </div>
                
            <!-- Agrega más items del carrusel aquí -->
        
            </div>
        
            <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Anterior</span>
            </a>
            
            <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Siguiente</span>
            </a>
        </div>
 
        <!-- Aquí se introducirá una pequeña presentación -->
        
        <div id="presentacion">
            <div class="container">
                <h1>¡Bienvenidos a SwiftVerse!</h1>
                <br>
                <p>SwiftVerse es la plataforma definitiva para los verdaderos "swifties" y amantes de la música. Sumérgete en la rica y fascinante carrera de Taylor Swift a través de una experiencia única que va más allá de la música. Descubre el impacto cultural, económico y emocional que ha dejado esta icónica artista en el mundo.</p>
            </div>
        </div>
    
    </div>
    

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.0.8/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    
    </div>
</div>
    
</body>
</html>