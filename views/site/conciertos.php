<?php

/** @var yii\web\View $this */

$this->title = 'Conciertos';

use yii\helpers\Html;
use yii\helpers\Url;

?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <!-- Integración de Bootstrap CSS -->
    <?= Html::cssFile('https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css') ?>
    <link rel="stylesheet" href="<?=Url::to('@web/css/conciertos.css')?>">
    <?php $this->head() ?>

</head>

<body>
    
    <div class="container-fluid" style="margin-top: 70px; width: 100vw;">
        <div class="row">
            <div class="col-md-12">
                <h1>TOURS</h1>
                
                <!-- Contenedor Fearless -->
                <div class="tour1" style="background-color: goldenrod">
                    <a href="#fearless"><?= Html::img('@web/images/tours/fearless_tour.jpg', ['alt' => 'Fearless']) ?></a>
                    <div class="tour1-content">
                        <h2>Fearless Tour</h2>
                        <p></p>
                        <button type="button" class="btn btn-dark">Ver conciertos</button>
                    </div>
                </div>
                
                <!-- Contenedor Speak now -->
                <div class="tour2" style="background-color: indigo;">
                    <a href="#speaknow"><?= Html::img('@web/images/tours/speaknow_tour.jpg', ['alt' => 'Speak now']) ?></a>
                    <h2>Speak Now Tour</h2>
                    <p></p>
                    <button type="button" class="btn btn-dark">Ver conciertos</button>
                </div>
                
                <!-- Contenedor Red -->
                <div class="tour1" style="background-color: darkred;">
                    <a href="#red"><?= Html::img('@web/images/tours/red_tour.jpg', ['alt' => 'Red']) ?></a>
                    <h2>Red</h2>
                    <p></p>
                    <button type="button" class="btn btn-dark">Ver conciertos</button>
                </div>
                
                <!-- Contenedor 1989 -->
                <div class="tour2" style="background-color: #17a2b8;">
                    <a href="#1989"><?= Html::img('@web/images/tours/1989_tour.jpg', ['alt' => 'Nineteen eighty nine']) ?></a>
                    <h2>1989 World Tour</h2>
                    <p></p>
                    <button type="button" class="btn btn-dark">Ver conciertos</button>
                </div>
                
                <!-- Contenedor Reputation -->
                <div class="tour1" style="background-color: #011627;">
                    <a href="#reputation"><?= Html::img('@web/images/tours/reputation_tour.png', ['alt' => 'Reputation']) ?></a>
                    <h2>Reputation Stadium Tour</h2>
                    <p></p>
                    <button type="button" class="btn btn-dark">Ver conciertos</button>
                </div>
                
                <!-- Contenedor Eras -->
                <div class="tour2" style="background-color: #003566;">
                    <a href="#ttpd"><?= Html::img('@web/images/tours/eras_tour.jpg', ['alt' => 'The Eras Tour']) ?></a>
                    <h2>The Eras Tour</h2>
                    <p></p>
                    <button type="button" class="btn btn-dark">Ver conciertos</button>
                </div>

            </div>
        </div>
    </div>
    
    <!-- Integración de Bootstrap JS y jQuery -->
    <?= Html::jsFile('https://cdn.jsdelivr.net/npm/jquery@3.7.1/dist/jquery.slim.min.js') ?>
    <?= Html::jsFile('https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js') ?>
    <?= Html::jsFile('https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js') ?>
    
    
</body>
</html>

