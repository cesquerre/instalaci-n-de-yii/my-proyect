<?php
use yii\helpers\Html;
use yii\helpers\Url;

/** @var yii\web\View $this */

$this->title = 'Álbumes';

?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <!-- Integración de Bootstrap CSS -->
    <?= Html::cssFile('https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css') ?>
    <link rel="stylesheet" href="<?=Url::to('@web/css/albumes.css')?>">
    <?php $this->head() ?>

</head>

<body>
    
    <div class="container-fluid" style="margin-top: 70px; width: 100vw;">
        <div class="row">
            <div class="col-lg-12">
                <h1>ÁLBUMES</h1>
                
                <!-- Contenedor Debut -->
                <div class="album1" style="background-color: lightgreen;">
                    <a class="active" href="#debut"><?= Html::img('@web/images/albumes/debut.jpg', ['alt' => 'Debut']) ?></a>
                    <h2>Debut</h2>
                    <p></p>
                    <button type="button" class="btn btn-dark">Ver canciones</button> <!-- Este botón dirigirá a canciones -->
                </div>
                
                <!-- Contenedor Fearless -->
                <div class="album2" style="background-color: goldenrod">
                    <a href="#fearless"><?= Html::img('@web/images/albumes/fearless.jpg', ['alt' => 'Fearless']) ?></a>
                    <h2>Fearless</h2>
                    <p></p>
                    <button type="button" class="btn btn-dark">Ver canciones</button>
                </div>
                
                <!-- Contenedor Speak now -->
                <div class="album1" style="background-color: indigo;">
                    <a href="#speaknow"><?= Html::img('@web/images/albumes/speaknow.jpg', ['alt' => 'Speak now']) ?></a>
                    <h2>Speak Now</h2>
                    <p></p>
                    <button type="button" class="btn btn-dark">Ver canciones</button>
                </div>
                
                <!-- Contenedor Red -->
                <div class="album2" style="background-color: darkred;">
                    <a href="#red"><?= Html::img('@web/images/albumes/red.jpg', ['alt' => 'Red']) ?></a>
                    <h2>Red</h2>
                    <p></p>
                    <button type="button" class="btn btn-dark">Ver canciones</button>
                </div>
                
                <!-- Contenedor 1989 -->
                <div class="album1" style="background-color: #5b85aa;">
                    <a href="#1989"><?= Html::img('@web/images/albumes/1989.jpg', ['alt' => 'Nineteen eighty nine']) ?></a>
                    <h2>1989</h2>
                    <p></p>
                    <button type="button" class="btn btn-dark">Ver canciones</button>
                </div>
                
                <!-- Contenedor Reputation -->
                <div class="album2" style="background-color: #060606;">
                    <a href="#reputation"><?= Html::img('@web/images/albumes/reputation.jpg', ['alt' => 'Reputation']) ?></a>
                    <h2>reputation</h2>
                    <p></p>
                    <button type="button" class="btn btn-dark">Ver canciones</button>
                </div>
                
                <!-- Contenedor Lover -->
                <div class="album1" style="background-color: #ffafcc;">
                    <a href="#lover"><?= Html::img('@web/images/albumes/lover.jpg', ['alt' => 'Lover']) ?></a>
                    <h2>Lover</h2>
                    <p></p>
                    <button type="button" class="btn btn-dark">Ver canciones</button>
                </div>
                
                <!-- Contenedor Folklore -->
                <div class="album2" style="background-color: gray;">
                    <a href="#folklore"><?= Html::img('@web/images/albumes/folklore.png', ['alt' => 'Folklore']) ?></a>
                    <h2>folklore</h2>
                    <p></p>
                    <button type="button" class="btn btn-dark">Ver canciones</button>
                </div>
                
                <!-- Contenedor Evermore -->
                <div class="album1" style="background-color: saddlebrown;">
                    <a href="#evermore"><?= Html::img('@web/images/albumes/evermore.png', ['alt' => 'Evermore']) ?></a>
                    <h2>evermore</h2>
                    <p></p>
                    <button type="button" class="btn btn-dark">Ver canciones</button>
                </div>
                
                <!-- Contenedor Midnights -->
                <div class="album2" style="background-color: #003566;">
                    <a href="#midnights"><?= Html::img('@web/images/albumes/midnights.jpg', ['alt' => 'Midnights']) ?></a>
                    <h2>Midnights</h2>
                    <p></p>
                    <button type="button" class="btn btn-dark">Ver canciones</button>
                </div>
                
                <!-- Contenedor TTPD -->
                <div class="album1" style="background-color: #625548;">
                    <a href="#ttpd"><?= Html::img('@web/images/albumes/ttpd.jpg', ['alt' => 'The Tortured Poets Department']) ?></a>
                    <h2>The Tortured Poets Department</h2>
                    <p></p>
                    <button type="button" class="btn btn-dark">Ver canciones</button>
                </div>

            </div>
        </div>
    </div>
    
    
    <!-- Integración de Bootstrap JS y jQuery -->
    <?= Html::jsFile('https://cdn.jsdelivr.net/npm/jquery@3.7.1/dist/jquery.slim.min.js') ?>
    <?= Html::jsFile('https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js') ?>
    <?= Html::jsFile('https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js') ?>

</body>
</html>

