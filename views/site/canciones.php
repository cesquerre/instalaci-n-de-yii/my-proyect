<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Canciones;
use yii\grid\GridView;

/** @var yii\web\View $this */

$this->title = 'Canciones';

?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <!-- Integración de Bootstrap CSS -->
    <?= Html::cssFile('https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css') ?>
    <link rel="stylesheet" href="<?=Url::to('@web/css/publicaciones.css')?>">
    <?php $this->head() ?>

</head>

<style>

h1{
   text-align: center;
}

.sidebar {
  margin: 0;
  padding: 0;
  width: 400px;
  background-color: #f8f9fa;
  position: fixed;
  height: 99%;
  overflow: auto;
}

/*Oculta el scrollbar*/
.sidebar::-webkit-scrollbar {
    display: none;
}

.sidebar a {
  display: block;
  padding: 25px;
}
 
.sidebar a.active {
  background-color: #201d38;
  color: white;
}

.sidebar a:hover:not(.active) {
  background-color: gray;
  color: white;
}

.canciones-container{
  margin-left: 450px;
  margin-top: 55px;
}

@media screen and (max-width: 700px) {
  .sidebar {
    width: 100%;
    height: auto;
    position: relative;
  }
  .sidebar a {float: left;}
  div.content {margin-left: 0;}
}

@media screen and (max-width: 400px) {
  .sidebar a {
    text-align: center;
    float: none;
  }
}
    
</style>


<body>
    
    <!-- Sidebar con fotos de las portadas -->
    <div class="sidebar">
        <a class="active" href="#debut"><?= Html::img('@web/images/albumes/debut.jpg', ['alt' => 'Debut', 'width' => '350', 'height' => '350']) ?></a>
        <a href="#fearless"><?= Html::img('@web/images/albumes/fearless.jpg', ['alt' => 'Fearless', 'width' => '350', 'height' => '350']) ?></a>
        <a href="#speaknow"><?= Html::img('@web/images/albumes/speaknow.jpg', ['alt' => 'Speak now', 'width' => '350', 'height' => '350']) ?></a>
        <a href="#red"><?= Html::img('@web/images/albumes/red.jpg', ['alt' => 'Red', 'width' => '350', 'height' => '350']) ?></a>
        <a href="#1989"><?= Html::img('@web/images/albumes/1989.jpg', ['alt' => 'Nineteen eighty nine', 'width' => '350', 'height' => '350']) ?></a>
        <a href="#reputation"><?= Html::img('@web/images/albumes/reputation.jpg', ['alt' => 'Reputation', 'width' => '350', 'height' => '350']) ?></a>
        <a href="#lover"><?= Html::img('@web/images/albumes/lover.jpg', ['alt' => 'Lover', 'width' => '350', 'height' => '350']) ?></a>
        <a href="#folklore"><?= Html::img('@web/images/albumes/folklore.png', ['alt' => 'Folklore', 'width' => '350', 'height' => '350']) ?></a>
        <a href="#evermore"><?= Html::img('@web/images/albumes/evermore.png', ['alt' => 'Evermore', 'width' => '350', 'height' => '350']) ?></a>
        <a href="#midnights"><?= Html::img('@web/images/albumes/midnights.jpg', ['alt' => 'Midnights', 'width' => '350', 'height' => '350']) ?></a>
        <a href="#ttpd"><?= Html::img('@web/images/albumes/ttpd.jpg', ['alt' => 'The Tortured Poets Department', 'width' => '350', 'height' => '350']) ?></a>
    </div>
    
    <div class="canciones-container">
        
        <h1>CANCIONES</h1>
        
        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id' => 'ID',
            'titulo' => 'Titulo',
            'duracion' => 'Duracion',
            [
            ],
        ],
    ]); ?>
        
    </div>
    
    
    <!-- Integración de Bootstrap JS y jQuery -->
    <?= Html::jsFile('https://cdn.jsdelivr.net/npm/jquery@3.7.1/dist/jquery.slim.min.js') ?>
    <?= Html::jsFile('https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js') ?>
    <?= Html::jsFile('https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js') ?>

</body>
</html>
