<?php

use yii\helpers\Html;

$this->title = "Artículos";

?>

<div class="article-container">
    <h4><?= Html::encode($model->titulo) ?></h4>
    <h5>Title description, Dec 7, 2017</h5>
    <p>Sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
    <button type="button" class="btn btn-dark">Leer más</button>
</div>
<br>
