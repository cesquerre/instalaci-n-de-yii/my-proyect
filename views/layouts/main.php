<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\helpers\Url;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
    
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="stylesheet" href="css/site.css">
    <link rel="icon" type="image/png" href="images/logo.png">
    
    <!--Integración de bootstrap-->
    
    <?= Html::cssFile('https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css') ?>
    
    <?php $this->head() ?>
</head>

<body class="d-flex flex-column h-100" style="background-color: #201d38; color: #e0e0e0; font-family: 'Neucha'">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-light bg-light fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => [
            ['label' => 'Álbumes', 'url' => ['site/albumes']],
            ['label' => 'Publicaciones', 'url' => ['/site/publicaciones']],
            ['label' => 'Artículos', 'url' => ['site/articulos']],
            ['label' => 'Conciertos', 'url' => ['site/conciertos']],
            ['label' => 'Merchandising', 'url' => ['site/merch']],
            [
                'label' => 'Administración',
                'items' =>[
                    ['label' => 'Álbumes', 'url' => ['/albumes/index']],
                    ['label' => 'Artículos', 'url' => ['/articulos/index']],
                    ['label' => 'Canciones', 'url' => ['/canciones/index']],
                    ['label' => 'Merchandising', 'url' => ['/merchandising/index']],
                    ['label' => 'Publicaciones', 'url' => ['/publicaciones/index']],
                    ['label' => 'Conciertos', 'url' => ['/conciertos/index']],
                    ['label' => 'Compositores', 'url' => ['/compositores/index']],
                    ['label' => 'Productores', 'url' => ['/productores/index']],
                    ['label' => 'Géneros Musicales', 'url' => ['/generos-musicales/index']],
                    ['label' => 'Precios', 'url' => ['/precios/index']],
                    ['label' => 'Colores', 'url' => ['/colores/index']],
                ],
            ],
        ],
    ]);
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0" style="overflow-x: hidden">
    <div class="container" style="margin: 0; padding: 0">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container">
        <p class="float-left">&copy; My Company <?= date('Y') ?></p>
        <p class="float-right"><?= Yii::powered() ?></p>
    </div>
</footer>
    
    <?= Html::jsFile('https://code.jquery.com/jquery-3.5.1.slim.min.js') ?>
    <?= Html::jsFile('https://cdn.jsdelivr.net/npm/@popperjs/core@2.0.8/dist/umd/popper.min.js') ?>
    <?= Html::jsFile('https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js') ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
