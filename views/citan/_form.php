<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Citan $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="citan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idPublicacion')->textInput() ?>

    <?= $form->field($model, 'idArticulo')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
