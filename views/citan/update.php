<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Citan $model */

$this->title = 'Update Citan: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Citans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<style>
    
    @import url('https://fonts.googleapis.com/css2?family=Neucha&display=swap');
        
    *{
        color: #e0e0e0;
        font-family: 'Neucha';
    }
    
    .citan-update{
        padding-left: 400px;
    }
    
</style>

<div class="citan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
