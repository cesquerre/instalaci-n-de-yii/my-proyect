<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "merchandising".
 *
 * @property int $id
 * @property int $codigo
 * @property string|null $modelo
 * @property float|null $precio
 *
 * @property Colores[] $colores
 * @property Albumes[] $idAlbums
 * @property Tienen[] $tienens
 */
class Merchandising extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'merchandising';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['codigo'], 'integer'],
            [['precio'], 'number'],
            [['modelo'], 'string', 'max' => 15],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo' => 'Codigo',
            'modelo' => 'Modelo',
            'precio' => 'Precio',
        ];
    }

    /**
     * Gets query for [[Colores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getColores()
    {
        return $this->hasMany(Colores::class, ['idMerch' => 'id']);
    }

    /**
     * Gets query for [[IdAlbums]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdAlbums()
    {
        return $this->hasMany(Albumes::class, ['id' => 'idAlbum'])->viaTable('tienen', ['idMerch' => 'id']);
    }

    /**
     * Gets query for [[Tienens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTienens()
    {
        return $this->hasMany(Tienen::class, ['idMerch' => 'id']);
    }
}
