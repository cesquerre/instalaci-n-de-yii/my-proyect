<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "colores".
 *
 * @property int $id
 * @property int|null $idMerch
 * @property string|null $color
 *
 * @property Merchandising $idMerch0
 */
class Colores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'colores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idMerch'], 'integer'],
            [['color'], 'string', 'max' => 20],
            [['idMerch', 'color'], 'unique', 'targetAttribute' => ['idMerch', 'color']],
            [['idMerch'], 'exist', 'skipOnError' => true, 'targetClass' => Merchandising::class, 'targetAttribute' => ['idMerch' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idMerch' => 'Id Merch',
            'color' => 'Color',
        ];
    }

    /**
     * Gets query for [[IdMerch0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdMerch0()
    {
        return $this->hasOne(Merchandising::class, ['id' => 'idMerch']);
    }
}
