<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "albumes".
 *
 * @property int $id
 * @property string $nombre
 * @property int|null $numcanciones
 *
 * @property Canciones[] $canciones
 * @property GenerosMusicales[] $generosMusicales
 * @property Conciertos[] $idConciertos
 * @property Merchandising[] $idMerches
 * @property Incluyen[] $incluyens
 * @property Publicaciones[] $publicaciones
 * @property Tienen[] $tienens
 */
class Albumes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'albumes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['numcanciones'], 'integer'],
            [['nombre'], 'string', 'max' => 10],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'numcanciones' => 'Numcanciones',
        ];
    }

    /**
     * Gets query for [[Canciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCanciones()
    {
        return $this->hasMany(Canciones::class, ['idAlbum' => 'id']);
    }

    /**
     * Gets query for [[GenerosMusicales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGenerosMusicales()
    {
        return $this->hasMany(GenerosMusicales::class, ['idAlbum' => 'id']);
    }

    /**
     * Gets query for [[IdConciertos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdConciertos()
    {
        return $this->hasMany(Conciertos::class, ['id' => 'idConcierto'])->viaTable('incluyen', ['idAlbum' => 'id']);
    }

    /**
     * Gets query for [[IdMerches]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdMerches()
    {
        return $this->hasMany(Merchandising::class, ['id' => 'idMerch'])->viaTable('tienen', ['idAlbum' => 'id']);
    }

    /**
     * Gets query for [[Incluyens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIncluyens()
    {
        return $this->hasMany(Incluyen::class, ['idAlbum' => 'id']);
    }

    /**
     * Gets query for [[Publicaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPublicaciones()
    {
        return $this->hasMany(Publicaciones::class, ['idAlbum' => 'id']);
    }

    /**
     * Gets query for [[Tienens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTienens()
    {
        return $this->hasMany(Tienen::class, ['idAlbum' => 'id']);
    }
}
