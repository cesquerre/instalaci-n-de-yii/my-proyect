<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "publicaciones".
 *
 * @property int $id
 * @property int $codigo
 * @property string $titulo
 * @property string $resumen
 * @property string $contenido
 * @property string|null $autor
 * @property string|null $fpublicacion
 * @property int|null $numpalabras
 * @property int|null $idAlbum
 *
 * @property Citan[] $citans
 * @property Albumes $idAlbum0
 * @property Articulos[] $idArticulos
 */
class Publicaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'publicaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'titulo', 'resumen', 'contenido'], 'required'],
            [['codigo', 'numpalabras', 'idAlbum'], 'integer'],
            [['fpublicacion'], 'safe'],
            [['titulo'], 'string', 'max' => 50],
            [['resumen'], 'string', 'max' => 100],
            [['contenido'], 'string', 'max' => 11000],
            [['autor'], 'string', 'max' => 30],
            [['codigo'], 'unique'],
            [['idAlbum'], 'exist', 'skipOnError' => true, 'targetClass' => Albumes::class, 'targetAttribute' => ['idAlbum' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo' => 'Codigo',
            'titulo' => 'Titulo',
            'resumen' => 'Resumen',
            'contenido' => 'Contenido',
            'autor' => 'Autor',
            'fpublicacion' => 'Fpublicacion',
            'numpalabras' => 'Numpalabras',
            'idAlbum' => 'Id Album',
        ];
    }

    /**
     * Gets query for [[Citans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCitans()
    {
        return $this->hasMany(Citan::class, ['idPublicacion' => 'id']);
    }

    /**
     * Gets query for [[IdAlbum0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdAlbum0()
    {
        return $this->hasOne(Albumes::class, ['id' => 'idAlbum']);
    }

    /**
     * Gets query for [[IdArticulos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdArticulos()
    {
        return $this->hasMany(Articulos::class, ['id' => 'idArticulo'])->viaTable('citan', ['idPublicacion' => 'id']);
    }
}
