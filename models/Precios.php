<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "precios".
 *
 * @property int $id
 * @property int|null $idConcierto
 * @property float|null $precio
 *
 * @property Conciertos $idConcierto0
 */
class Precios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'precios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idConcierto'], 'integer'],
            [['precio'], 'number'],
            [['idConcierto', 'precio'], 'unique', 'targetAttribute' => ['idConcierto', 'precio']],
            [['idConcierto'], 'exist', 'skipOnError' => true, 'targetClass' => Conciertos::class, 'targetAttribute' => ['idConcierto' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idConcierto' => 'Id Concierto',
            'precio' => 'Precio',
        ];
    }

    /**
     * Gets query for [[IdConcierto0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdConcierto0()
    {
        return $this->hasOne(Conciertos::class, ['id' => 'idConcierto']);
    }
}
