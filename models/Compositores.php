<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "compositores".
 *
 * @property int $id
 * @property int|null $idCancion
 * @property string|null $compositor
 *
 * @property Canciones $idCancion0
 */
class Compositores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'compositores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idCancion'], 'integer'],
            [['compositor'], 'string', 'max' => 30],
            [['idCancion', 'compositor'], 'unique', 'targetAttribute' => ['idCancion', 'compositor']],
            [['idCancion'], 'exist', 'skipOnError' => true, 'targetClass' => Canciones::class, 'targetAttribute' => ['idCancion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idCancion' => 'Id Cancion',
            'compositor' => 'Compositor',
        ];
    }

    /**
     * Gets query for [[IdCancion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdCancion0()
    {
        return $this->hasOne(Canciones::class, ['id' => 'idCancion']);
    }
}
