<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "articulos".
 *
 * @property int $id
 * @property int $codigo
 * @property string|null $autor
 * @property string|null $titulo
 * @property string $enlace
 * @property string|null $fpublicacion
 * @property int|null $numpaginas
 *
 * @property Citan[] $citans
 * @property Publicaciones[] $idPublicacions
 */
class Articulos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'articulos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'enlace'], 'required'],
            [['codigo', 'numpaginas'], 'integer'],
            [['fpublicacion'], 'safe'],
            [['autor'], 'string', 'max' => 30],
            [['titulo'], 'string', 'max' => 50],
            [['enlace'], 'string', 'max' => 800],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo' => 'Codigo',
            'autor' => 'Autor',
            'titulo' => 'Titulo',
            'enlace' => 'Enlace',
            'fpublicacion' => 'Fpublicacion',
            'numpaginas' => 'Numpaginas',
        ];
    }

    /**
     * Gets query for [[Citans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCitans()
    {
        return $this->hasMany(Citan::class, ['idArticulo' => 'id']);
    }

    /**
     * Gets query for [[IdPublicacions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPublicacions()
    {
        return $this->hasMany(Publicaciones::class, ['id' => 'idPublicacion'])->viaTable('citan', ['idArticulo' => 'id']);
    }
}
