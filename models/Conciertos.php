<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "conciertos".
 *
 * @property int $id
 * @property int $codigo
 * @property string|null $tour
 * @property string|null $ubicacion
 * @property string|null $fecha
 * @property int|null $capacidad
 *
 * @property Precios[] $precios
 */
class Conciertos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'conciertos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['codigo', 'capacidad'], 'integer'],
            [['tour'], 'string'],
            [['fecha'], 'safe'],
            [['ubicacion'], 'string', 'max' => 80],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo' => 'Codigo',
            'tour' => 'Tour',
            'ubicacion' => 'Ubicacion',
            'fecha' => 'Fecha',
            'capacidad' => 'Capacidad',
        ];
    }

    /**
     * Gets query for [[Precios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPrecios()
    {
        return $this->hasMany(Precios::class, ['idConcierto' => 'id']);
    }
}
