<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "citan".
 *
 * @property int $id
 * @property int|null $idPublicacion
 * @property int|null $idArticulo
 *
 * @property Articulos $idArticulo0
 * @property Publicaciones $idPublicacion0
 */
class Citan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'citan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idPublicacion', 'idArticulo'], 'integer'],
            [['idPublicacion', 'idArticulo'], 'unique', 'targetAttribute' => ['idPublicacion', 'idArticulo']],
            [['idArticulo'], 'exist', 'skipOnError' => true, 'targetClass' => Articulos::class, 'targetAttribute' => ['idArticulo' => 'id']],
            [['idPublicacion'], 'exist', 'skipOnError' => true, 'targetClass' => Publicaciones::class, 'targetAttribute' => ['idPublicacion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idPublicacion' => 'Id Publicacion',
            'idArticulo' => 'Id Articulo',
        ];
    }

    /**
     * Gets query for [[IdArticulo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdArticulo0()
    {
        return $this->hasOne(Articulos::class, ['id' => 'idArticulo']);
    }

    /**
     * Gets query for [[IdPublicacion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPublicacion0()
    {
        return $this->hasOne(Publicaciones::class, ['id' => 'idPublicacion']);
    }
}
