<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tienen".
 *
 * @property int $id
 * @property int|null $idAlbum
 * @property int|null $idMerch
 *
 * @property Albumes $idAlbum0
 * @property Merchandising $idMerch0
 */
class Tienen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tienen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idAlbum', 'idMerch'], 'integer'],
            [['idAlbum', 'idMerch'], 'unique', 'targetAttribute' => ['idAlbum', 'idMerch']],
            [['idAlbum'], 'exist', 'skipOnError' => true, 'targetClass' => Albumes::class, 'targetAttribute' => ['idAlbum' => 'id']],
            [['idMerch'], 'exist', 'skipOnError' => true, 'targetClass' => Merchandising::class, 'targetAttribute' => ['idMerch' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idAlbum' => 'Id Album',
            'idMerch' => 'Id Merch',
        ];
    }

    /**
     * Gets query for [[IdAlbum0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdAlbum0()
    {
        return $this->hasOne(Albumes::class, ['id' => 'idAlbum']);
    }

    /**
     * Gets query for [[IdMerch0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdMerch0()
    {
        return $this->hasOne(Merchandising::class, ['id' => 'idMerch']);
    }
}
