<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "generos_musicales".
 *
 * @property int $id
 * @property int|null $idAlbum
 * @property string|null $genero
 *
 * @property Albumes $idAlbum0
 */
class GenerosMusicales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'generos_musicales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idAlbum'], 'integer'],
            [['genero'], 'string', 'max' => 15],
            [['idAlbum', 'genero'], 'unique', 'targetAttribute' => ['idAlbum', 'genero']],
            [['idAlbum'], 'exist', 'skipOnError' => true, 'targetClass' => Albumes::class, 'targetAttribute' => ['idAlbum' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idAlbum' => 'Id Album',
            'genero' => 'Genero',
        ];
    }

    /**
     * Gets query for [[IdAlbum0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdAlbum0()
    {
        return $this->hasOne(Albumes::class, ['id' => 'idAlbum']);
    }
}
