<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "canciones".
 *
 * @property int $id
 * @property string $titulo
 * @property string|null $duracion
 * @property int|null $idAlbum
 *
 * @property Compositores[] $compositores
 * @property Albumes $idAlbum0
 * @property Productores[] $productores
 */
class Canciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'canciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo'], 'required'],
            [['idAlbum'], 'integer'],
            [['titulo'], 'string', 'max' => 30],
            [['duracion'], 'string', 'max' => 5],
            [['titulo'], 'unique'],
            [['idAlbum'], 'exist', 'skipOnError' => true, 'targetClass' => Albumes::class, 'targetAttribute' => ['idAlbum' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'duracion' => 'Duracion',
            'idAlbum' => 'Id Album',
        ];
    }

    /**
     * Gets query for [[Compositores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompositores()
    {
        return $this->hasMany(Compositores::class, ['idCancion' => 'id']);
    }

    /**
     * Gets query for [[IdAlbum0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdAlbum0()
    {
        return $this->hasOne(Albumes::class, ['id' => 'idAlbum']);
    }

    /**
     * Gets query for [[Productores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductores()
    {
        return $this->hasMany(Productores::class, ['idCancion' => 'id']);
    }
}
