<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productores".
 *
 * @property int $id
 * @property int|null $idCancion
 * @property string|null $productor
 *
 * @property Canciones $idCancion0
 */
class Productores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idCancion'], 'integer'],
            [['productor'], 'string', 'max' => 30],
            [['idCancion', 'productor'], 'unique', 'targetAttribute' => ['idCancion', 'productor']],
            [['idCancion'], 'exist', 'skipOnError' => true, 'targetClass' => Canciones::class, 'targetAttribute' => ['idCancion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idCancion' => 'Id Cancion',
            'productor' => 'Productor',
        ];
    }

    /**
     * Gets query for [[IdCancion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdCancion0()
    {
        return $this->hasOne(Canciones::class, ['id' => 'idCancion']);
    }
}
